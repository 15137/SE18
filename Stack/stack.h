#include<iostream>
#include<assert.h>
using namespace std;
 
struct stack 
{
  int *arr;
   int top;
  int size_s = -1 ;
};
typedef struct stack Stk;

void init(Stk *s,int n);
void push(Stk *s,int ele);
void pop(Stk *s);
int peek(Stk *s);
